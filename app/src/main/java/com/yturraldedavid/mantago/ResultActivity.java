package com.yturraldedavid.mantago;

import android.content.Context;
import android.content.Intent;
import android.media.Image;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;


public class ResultActivity extends AppCompatActivity {

    RecyclerView recyclerViewLugares;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);
        recyclerViewLugares = (RecyclerView) findViewById(R.id.recyclerViewLugares);
        recyclerViewLugares.setLayoutManager(new LinearLayoutManager(this));
        final ArrayList<LugaresTuristicos> listaLugares = new ArrayList<>();
        int opcion = getIntent().getExtras().getInt("opcion");
        switch (opcion){
            case 1:
                listaLugares.add(new LugaresTuristicos("Museo Cancebí",R.drawable.lt01));
            case 2:
                listaLugares.add(new LugaresTuristicos("Playa Murcielago", R.drawable.p01));
                listaLugares.add(new LugaresTuristicos("Playita Mia", R.drawable.p02));
                break;

            case 3:
                listaLugares.add(new LugaresTuristicos("Martinica", R.drawable.r01));
                listaLugares.add(new LugaresTuristicos("Chamaco", R.drawable.r02));
                break;
            case 4:
                listaLugares.add(new LugaresTuristicos("Wild Bar", R.drawable.b01));
                listaLugares.add(new LugaresTuristicos("Noa Bar", R.drawable.b02));
                break;
            case 5:
                listaLugares.add(new LugaresTuristicos("Oro verde", R.drawable.h01));
                listaLugares.add(new LugaresTuristicos("Balandra", R.drawable.h02));

        }
        ResultListAdapter adapter = new ResultListAdapter(listaLugares);
        adapter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int opcion=0;
                Intent intent = new Intent(ResultActivity.this, DetailActivity.class);
                switch (opcion){
                    case 1:
                        opcion = 1;
                        break;
                    case 2:
                        opcion = 2;
                        break;
                    case 3:
                        opcion = 3;
                        break;
                    case 4:
                        opcion = 4;
                        break;
                    case 5:
                        opcion = 5;
                        break;
                    case 6:
                        opcion = 6;
                        break;
                    case 7:
                        opcion = 7;
                        break;
                    case 8:
                        opcion = 8;
                        break;
                    case 9:
                        opcion = 9;
                        break;


                }

                Toast.makeText(getApplicationContext(), "Selección: "+listaLugares.get
                        (recyclerViewLugares.getChildAdapterPosition(v))
                        .getNombre(),Toast.LENGTH_SHORT).show();
            }
        });
        recyclerViewLugares.setAdapter(adapter);

    }



}
