package com.yturraldedavid.mantago;

public class LugaresTuristicos {
    private String nombre;
    private int foto;
    private String descripcion;

    public LugaresTuristicos(){}

    public LugaresTuristicos(String nombre, int foto) {
        this.nombre = nombre;
        this.foto = foto;
    }

    public LugaresTuristicos(String nombre, int foto, String descripcion) {
        this.nombre = nombre;
        this.foto = foto;
        this.descripcion = descripcion;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getFoto() {
        return foto;
    }

    public void setFoto(int foto) {
        this.foto = foto;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
}
